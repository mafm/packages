# Translation of sections to French
# Copyright (C) 2005, 2010 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the package.debian.org website.
#
# Guilhelm Panaget <guilhelm.panaget@free.fr>, 2005.
# David Prévot <david@tilapin.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: 1.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-12-11 15:50+0800\n"
"PO-Revision-Date: 2010-08-29 00:44-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: lib/Packages/Sections.pm:12
msgid "Administration Utilities"
msgstr "Outils d'administration"

#: lib/Packages/Sections.pm:13
msgid "Utilities to administer system resources, manage user accounts, etc."
msgstr ""
"Outils pour administrer les ressources du système, gérer les comptes "
"utilisateur, etc."

#: lib/Packages/Sections.pm:14
msgid "Base Utilities"
msgstr "Outils de base"

#: lib/Packages/Sections.pm:15
msgid "Basic needed utilities of every Debian system."
msgstr "Outils de base nécessaires à tout système Debian"

#: lib/Packages/Sections.pm:16
msgid "Mono/CLI"
msgstr "Mono et CLI"

#: lib/Packages/Sections.pm:17
msgid "Everything about Mono and the Common Language Infrastructure."
msgstr "Tout ce qui concerne Mono et le Common Language Infrastructure."

#: lib/Packages/Sections.pm:18
msgid "Communication Programs"
msgstr "Programmes pour outils de communication"

#: lib/Packages/Sections.pm:19
msgid "Software to use your modem in the old fashioned style."
msgstr "Programmes pour utiliser votre modem à l'ancienne."

#: lib/Packages/Sections.pm:20
msgid "Databases"
msgstr "Bases de données"

#: lib/Packages/Sections.pm:21
msgid "Database Servers and Clients."
msgstr "Clients et serveurs de bases de données."

#: lib/Packages/Sections.pm:22
msgid "Debug packages"
msgstr "Paquets de débogage"

#: lib/Packages/Sections.pm:23
msgid ""
"Packages providing debugging information for executables and shared "
"libraries."
msgstr ""
"Paquets fournissant des renseignement de débogage pour les exécutables et "
"les bibliothèques partagées."

#: lib/Packages/Sections.pm:24
msgid "Development"
msgstr "Développement"

#: lib/Packages/Sections.pm:25
msgid ""
"Development utilities, compilers, development environments, libraries, etc."
msgstr ""
"Outils de développement, compilateurs, environnements de développement, "
"bibliothèques, etc."

#: lib/Packages/Sections.pm:26
msgid "Documentation"
msgstr "Documentation"

#: lib/Packages/Sections.pm:27
msgid ""
"FAQs, HOWTOs and other documents trying to explain everything related to "
"Debian, and software needed to browse documentation (man, info, etc)."
msgstr ""
"FAQ, HOWTO et autres documents permettant d'expliquer tout ce qui concerne "
"Debian ainsi que les programmes pour naviguer dans la documentation (man, "
"info, etc)."

#: lib/Packages/Sections.pm:28
msgid "Editors"
msgstr "Éditeurs"

#: lib/Packages/Sections.pm:29
msgid "Software to edit files. Programming environments."
msgstr "Programme pour éditer des fichiers. Environnements de développement."

#: lib/Packages/Sections.pm:30
msgid "Education"
msgstr ""

#: lib/Packages/Sections.pm:31
#, fuzzy
#| msgid "Software for ham radio."
msgid "Software for learning and teaching."
msgstr "Logiciels pour radioamateurs."

#: lib/Packages/Sections.pm:32
msgid "Electronics"
msgstr "Électronique"

#: lib/Packages/Sections.pm:33
msgid "Electronics utilities."
msgstr "Outils pour l'électronique."

#: lib/Packages/Sections.pm:34
msgid "Embedded software"
msgstr "Logiciel embarqué"

#: lib/Packages/Sections.pm:35
msgid "Software suitable for use in embedded applications."
msgstr "Logiciels utilisables dans des applications embarquées."

#: lib/Packages/Sections.pm:36
msgid "Fonts"
msgstr "Polices"

#: lib/Packages/Sections.pm:37
msgid "Font packages."
msgstr "Paquets de police."

#: lib/Packages/Sections.pm:38
msgid "Games"
msgstr "Jeux"

#: lib/Packages/Sections.pm:39
msgid "Programs to spend a nice time with after all this setting up."
msgstr "Programmes pour se détendre un peu après toute cette configuration."

#: lib/Packages/Sections.pm:40
msgid "GNOME"
msgstr "GNOME"

#: lib/Packages/Sections.pm:41
msgid ""
"The GNOME desktop environment, a powerful, easy to use set of integrated "
"applications."
msgstr ""
"L'environnement de bureau GNOME, un ensemble puissant d'applications "
"intégrées facile à utiliser."

#: lib/Packages/Sections.pm:42
msgid "GNU R"
msgstr "GNU R"

#: lib/Packages/Sections.pm:43
msgid "Everything about GNU R, a statistical computation and graphics system."
msgstr ""
"Tout ce qui concerne GNU R, analyse statistique et fonctions graphiques."

#: lib/Packages/Sections.pm:44
msgid "GNUstep"
msgstr "GNUstep"

#: lib/Packages/Sections.pm:45
msgid "The GNUstep environment."
msgstr "L'environnement de bureau GNUstep."

#: lib/Packages/Sections.pm:46
msgid "Graphics"
msgstr "Graphisme"

#: lib/Packages/Sections.pm:47
msgid "Editors, viewers, converters... Everything to become an artist."
msgstr ""
"Éditeurs, visionneuses, convertisseurs... Tout pour faire de vous un artiste."

#: lib/Packages/Sections.pm:48
msgid "Ham Radio"
msgstr "Radioamateur"

#: lib/Packages/Sections.pm:49
msgid "Software for ham radio."
msgstr "Logiciels pour radioamateurs."

#: lib/Packages/Sections.pm:50
msgid "Haskell"
msgstr "Haskell"

#: lib/Packages/Sections.pm:51
msgid "Everything about Haskell."
msgstr "Tout ce qui concerne Haskell."

#: lib/Packages/Sections.pm:52
msgid "Web Servers"
msgstr "Serveurs web"

#: lib/Packages/Sections.pm:53
msgid "Web servers and their modules."
msgstr "Serveurs web et leurs modules."

#: lib/Packages/Sections.pm:54
msgid "Interpreters"
msgstr "Interpréteurs"

#: lib/Packages/Sections.pm:55
msgid "All kind of interpreters for interpreted languages. Macro processors."
msgstr ""
"Toute sorte d'interpréteurs pour langages interprétés et langages de macros."

#: lib/Packages/Sections.pm:56
msgid "Introspection"
msgstr ""

#: lib/Packages/Sections.pm:57
msgid "Machine readable introspection data for use by development tools."
msgstr ""

#: lib/Packages/Sections.pm:58
msgid "Java"
msgstr "Java"

#: lib/Packages/Sections.pm:59
msgid "Everything about Java."
msgstr "Tout ce qui concerne Java."

#: lib/Packages/Sections.pm:60
msgid "JavaScript"
msgstr ""

#: lib/Packages/Sections.pm:61
msgid "JavaScript programming language, libraries, and development tools."
msgstr ""

#: lib/Packages/Sections.pm:62
msgid "KDE"
msgstr "KDE"

#: lib/Packages/Sections.pm:63
msgid ""
"The K Desktop Environment, a powerful, easy to use set of integrated "
"applications."
msgstr ""
"L'environnement de bureau KDE, un ensemble puissant d'applications intégrées "
"facile à utiliser."

#: lib/Packages/Sections.pm:64
msgid "Kernels"
msgstr "Noyaux"

#: lib/Packages/Sections.pm:65
msgid "Operating System Kernels and related modules."
msgstr "Noyaux du système d'exploitation et modules associés."

#: lib/Packages/Sections.pm:66
msgid "Library development"
msgstr "Fichiers de développement pour les bibliothèques."

#: lib/Packages/Sections.pm:67
msgid "Libraries necessary for developers to write programs that use them."
msgstr "Fichiers nécessaires aux développeurs pour utiliser les bibliothèques."

#: lib/Packages/Sections.pm:68
msgid "Libraries"
msgstr "Bibliothèques"

#: lib/Packages/Sections.pm:69
msgid ""
"Libraries to make other programs work. They provide special features to "
"developers."
msgstr ""
"Bibliothèques utilisées par d'autres programmes et fournissant des "
"fonctionnalités aux programmeurs."

#: lib/Packages/Sections.pm:70
msgid "Lisp"
msgstr "Lisp"

#: lib/Packages/Sections.pm:71
msgid "Everything about Lisp."
msgstr "Tout ce qui concerne Lisp."

#: lib/Packages/Sections.pm:72
msgid "Language packs"
msgstr "Paquets de langue."

#: lib/Packages/Sections.pm:73
msgid "Localization support for big software packages."
msgstr "Prise en charge de la localisation pour les gros paquets logiciels."

#: lib/Packages/Sections.pm:74
msgid "Mail"
msgstr "Courrier électronique"

#: lib/Packages/Sections.pm:75
msgid "Programs to route, read, and compose E-mail messages."
msgstr "Logiciels pour envoyer, lire et rédiger des courriers électroniques."

#: lib/Packages/Sections.pm:76
msgid "Mathematics"
msgstr "Mathématiques"

#: lib/Packages/Sections.pm:77
msgid "Math software."
msgstr "Logiciels de mathématiques."

#: lib/Packages/Sections.pm:78
#, fuzzy
#| msgid "Virtual packages"
msgid "Meta packages"
msgstr "Paquets virtuels"

#: lib/Packages/Sections.pm:79
msgid "Packages that mainly provide dependencies on other packages."
msgstr ""

#: lib/Packages/Sections.pm:80
msgid "Miscellaneous"
msgstr "Divers"

#: lib/Packages/Sections.pm:81
msgid "Miscellaneous utilities that didn't fit well anywhere else."
msgstr "Outils en tout genre impossible à caser ailleurs."

#: lib/Packages/Sections.pm:82
msgid "Network"
msgstr "Réseaux"

#: lib/Packages/Sections.pm:83
msgid "Daemons and clients to connect your system to the world."
msgstr "Démons et clients pour connecter votre système à l'extérieur."

#: lib/Packages/Sections.pm:84
msgid "Newsgroups"
msgstr "Forums de discussion"

#: lib/Packages/Sections.pm:85
msgid "Software to access Usenet, to set up news servers, etc."
msgstr ""
"Logiciels permettant d'accéder à Usenet, de mettre en place un serveur de "
"nouvelles, etc."

#: lib/Packages/Sections.pm:86
msgid "OCaml"
msgstr "OCaml"

#: lib/Packages/Sections.pm:87
msgid "Everything about OCaml, an ML language implementation."
msgstr "Tout ce qui concerne OCaml, une implémentation de language ML."

#: lib/Packages/Sections.pm:88
msgid "Old Libraries"
msgstr "Bibliothèques obsolètes"

#: lib/Packages/Sections.pm:89
msgid ""
"Old versions of libraries, kept for backward compatibility with old "
"applications."
msgstr ""
"Versions obsolètes de bibliothèques, conservées à des fins de compatibilité "
"avec d'anciennes applications."

#: lib/Packages/Sections.pm:90
msgid "Other OS's and file systems"
msgstr "Autres systèmes d'exploitation et systèmes de fichiers"

#: lib/Packages/Sections.pm:91
msgid ""
"Software to run programs compiled for other operating systems, and to use "
"their filesystems."
msgstr ""
"Logiciels permettant d'exécuter des programmes compilés pour d'autres "
"systèmes d'exploitation et d'utiliser leur système de fichiers."

#: lib/Packages/Sections.pm:92
msgid "Perl"
msgstr "Perl"

#: lib/Packages/Sections.pm:93
msgid "Everything about Perl, an interpreted scripting language."
msgstr "Tout ce qui concerne Perl, langage interprété pour scripts."

#: lib/Packages/Sections.pm:94
msgid "PHP"
msgstr "PHP"

#: lib/Packages/Sections.pm:95
msgid "Everything about PHP."
msgstr "Tout ce qui concerne PHP."

#: lib/Packages/Sections.pm:96
msgid "Python"
msgstr "Python"

#: lib/Packages/Sections.pm:97
msgid ""
"Everything about Python, an interpreted, interactive object oriented "
"language."
msgstr ""
"Tout ce qui concerne Python, langage interprété, interactif et orienté objet."

#: lib/Packages/Sections.pm:98
msgid "Ruby"
msgstr "Ruby"

#: lib/Packages/Sections.pm:99
msgid "Everything about Ruby, an interpreted object oriented language."
msgstr "Tout ce qui concerne Ruby, langage interprété et orienté objet."

#: lib/Packages/Sections.pm:100
msgid "Rust"
msgstr ""

#: lib/Packages/Sections.pm:101
msgid "Rust programming language, library crates, and development tools"
msgstr ""

#: lib/Packages/Sections.pm:102
msgid "Science"
msgstr "Science"

#: lib/Packages/Sections.pm:103
msgid "Basic tools for scientific work"
msgstr "Outils de base pour les travaux scientifiques."

#: lib/Packages/Sections.pm:104
msgid "Shells"
msgstr "Shells"

#: lib/Packages/Sections.pm:105
msgid "Command shells. Friendly user interfaces for beginners."
msgstr "Shells de commandes. Interfaces abordables par le débutant."

#: lib/Packages/Sections.pm:106
msgid "Sound"
msgstr "Son"

#: lib/Packages/Sections.pm:107
msgid ""
"Utilities to deal with sound: mixers, players, recorders, CD players, etc."
msgstr ""
"Outils pour travailler avec le son : tables de mixage, lecteurs, "
"enregistreurs, lecteurs de CD, etc."

#: lib/Packages/Sections.pm:108
msgid "Tasks"
msgstr ""

#: lib/Packages/Sections.pm:109
msgid ""
"Packages that are used by 'tasksel', a simple interface for users who want "
"to configure their system to perform a specific task."
msgstr ""

#: lib/Packages/Sections.pm:110
msgid "TeX"
msgstr "TeX"

#: lib/Packages/Sections.pm:111
msgid "The famous typesetting software and related programs."
msgstr "Le célèbre logiciel de composition et les programmes associés."

#: lib/Packages/Sections.pm:112
msgid "Text Processing"
msgstr "Traitement de texte"

#: lib/Packages/Sections.pm:113
msgid "Utilities to format and print text documents."
msgstr "Outils pour la mise en forme et l'impression de documents texte."

#: lib/Packages/Sections.pm:114
msgid "Translations"
msgstr "Traductions"

#: lib/Packages/Sections.pm:115
msgid "Translation packages and language support meta packages."
msgstr "Paquets de traduction et méta-paquets de gestion des langues"

#: lib/Packages/Sections.pm:116
msgid "Utilities"
msgstr "Outils système"

#: lib/Packages/Sections.pm:117
msgid ""
"Utilities for file/disk manipulation, backup and archive tools, system "
"monitoring, input systems, etc."
msgstr ""
"Outils de manipulation de fichiers ou de disques, de sauvegarde et "
"d'archivage, de surveillance du système, systèmes d'entrée, etc."

#: lib/Packages/Sections.pm:118
msgid "Version Control Systems"
msgstr "Systèmes de gestion de version"

#: lib/Packages/Sections.pm:119
msgid "Version control systems and related utilities."
msgstr "Systèmes de gestion de version et utilitaires associés."

#: lib/Packages/Sections.pm:120
msgid "Video"
msgstr "Vidéo"

#: lib/Packages/Sections.pm:121
msgid "Video viewers, editors, recording, streaming."
msgstr "Lecture, édition, enregistrement et flux vidéo."

#: lib/Packages/Sections.pm:122
msgid "Virtual packages"
msgstr "Paquets virtuels"

#: lib/Packages/Sections.pm:123
msgid "Virtual packages."
msgstr "Paquets virtuels"

#: lib/Packages/Sections.pm:124
msgid "Web Software"
msgstr "Logiciels pour le Web"

#: lib/Packages/Sections.pm:125
msgid "Web servers, browsers, proxies, download tools etc."
msgstr ""
"Serveurs Web, navigateurs, serveurs mandataires, outils de téléchargement, "
"etc."

#: lib/Packages/Sections.pm:126
msgid "X Window System software"
msgstr "Logiciels pour le système X Window"

#: lib/Packages/Sections.pm:127
msgid ""
"X servers, libraries, fonts, window managers, terminal emulators and many "
"related applications."
msgstr ""
"Serveurs X, bibliothèques, gestionnaires de fenêtres, émulateurs de terminal "
"et autres applications associées."

#: lib/Packages/Sections.pm:128
msgid "Xfce"
msgstr "Xfce"

#: lib/Packages/Sections.pm:129
msgid "Xfce, a fast and lightweight Desktop Environment."
msgstr "Environnement de bureau Xfce, léger et rapide."

#: lib/Packages/Sections.pm:130
msgid "Zope/Plone Framework"
msgstr "Système Zope et Plone"

#: lib/Packages/Sections.pm:131
msgid "Zope Application Server and Plone Content Managment System."
msgstr "Serveur d'application Zope et système de gestion de contenu Plone."

#: lib/Packages/Sections.pm:132
msgid "debian-installer udeb packages"
msgstr "Paquets udeb de l'installateur Debian"

#: lib/Packages/Sections.pm:133
msgid ""
"Special packages for building customized debian-installer variants. Do not "
"install them on a normal system!"
msgstr ""
"Paquets spécifiques pour la création de variantes personnalisées de "
"l'installateur Debian. Ne pas installer sur un système normal !"
